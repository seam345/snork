# Project Snork

Managing various sensors in Poppy’s house with alerts triggered on certain events and storage for historical data lookup.

Named after the Moomin character and not the noise.

_Snork (Finnish: Niisku; Swedish: Snorken) is a character who first appears in the Moomin books. In the books, the Snork is an ordinary person whose primary characteristic is his love of order and being in charge, but in the 1990 TV series, he is a professional inventor and scientist._ - MoominWiki

## High Level Goals
- [ ] Setup a zigbee/mqtt/grafana/prometheus (or some other time series database)
- [ ] Remote deployment with nix/krops
- [ ] Give Poppy ownership, but keep Sean with full access that can easily be revoked at a moments notice
- [ ] Access from outside of LAN
- [ ] Smart alerts to various things (preferably push notifications on phone)
- [ ] Have a data backup of time series database, and every other state database (node red/ grafana/ zigbee2mqtt)

## Learning Objectives 
### Sean
- [ ] Security of system
- [ ] Handover of ownership
- [ ] Managing strip lights with wled (likely to require some custom wiring/soldering due to 3.3v controller and 5v leds)
- [ ] Create a custom zigbee sensor to monitor locked door
- [ ] In depth understand of differences between various time series databases (prometheus/open TSSB /influx db/**open to others to look at**)

### Poppy
- [ ] Crash course on nixos
- [ ] Zigbee2mqtt 
- [ ] Node red
- [ ] Time series database


## Data to Capture 
- [ ] Humidity
- [ ] Temperature
- [ ] Doors/windows open
- [ ] Lights dimmable to set with fake sunrise/sunset
### Extra
- [ ] Doors/windows locked
- [ ] Electricity and gas metering - currently not possible, apparently it's being worked on...
- [ ] Fridge/freezer door open (maybe)

## Hardware
- [ ] 1 rpi 4 8GB
- [ ] 6x aqara door sensor
  - Single £16.99 https://amzn.eu/d/fhtXvKH
  - 3 pack £47.99 https://amzn.eu/d/alyLeGV
- [ ] >2 aqara temprature and humidity  sensors 
  - Single £19 https://amzn.eu/d/bXcIfk5
  - 3 pack for £55 https://www.amazon.co.uk/dp/B09WRB33KM/
  - bit unsure on these i cant reduce the report time to 5min
- [ ] Dimmable light bulb (TBC)

## Extra Stretch
- [ ] Electric and gas metering - currently not possible to get this direct from smart meter, apparently it's being worked on...
- [ ] Operate the radiators to reduce gas bill for heating. Base this on ^ and temperature sensor data.
- [ ] Build a fermentation chamber that controls temp.
- [ ] Build a mushroom growing chamber that controls temperature and humidity.

## Game Plan
- [ ] Set up pi with nixos (Snork)
- [ ] Get pi running zigbee2mqtt in docker
- [ ] Get sensors talking to Snork
- [ ] Set up prometheus on pi
- [ ] Prometheus absorbs mqtt messages
- [ ] Set up grafana to use prometheus database
- [ ] Set up node red to use mqtt messages to trigger automations
- [ ] Investigate / Set up backup options
